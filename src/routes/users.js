const { Router } = require('express');
const router = Router();
const fetch = require('node-fetch');

router.get('/', (req, res) => {
	fetch('https://jsonplaceholder.typicode.com/users')
		.then((data) => data.json())
		.then((users) => {
			res.json(users);
		})
		.catch((err) => {
			res.status(500).json({ error: 'There was an error.' });
		});
});

module.exports = router;
